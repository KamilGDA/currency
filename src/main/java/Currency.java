public class Currency {

    public Currency(final String code, final String mid, final String currency) {
        this.code = code;
        this.table = mid;
        this.currency = currency;
    }

    public String table;
    public String currency;
    public String code;
    public Rate[] rates;

    public String getCode() {
        return code;
    }

    public String getCurrency() {
        return currency;
    }

    public String getTable() {
        return currency;
    }

}