import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class Main {
    public static String downloadJsonFromURL(String urlText) {
        try {
            URL myUrl = new URL(urlText);
            StringBuilder jsonText = new StringBuilder();
            try (InputStream myInputStream = myUrl.openStream();
                 Scanner scanner = new Scanner(myInputStream)) {
                while (scanner.hasNextLine()) {
                    jsonText.append(scanner.nextLine());
                }
                return jsonText.toString();
            }
        } catch (IOException e) {
            System.err.println("Failed to get content from URL " + urlText + " due to exception: " + e.getMessage());
            return null;
        }
    }

    public static void main(String[] args) {

        List<String> currencyCodes = Arrays.asList("EUR", "CHF", "USD", "GBP");
        for (String code : currencyCodes) {
            String url = downloadJsonFromURL("http://api.nbp.pl/api/exchangerates/rates/a/" + code + "?format=json");
            Gson gson = new Gson();
            Currency eurCurrency = gson.fromJson(url, Currency.class);
            double eurRateValue = eurCurrency.rates[0].mid;
            NumberFormat formatter = new DecimalFormat("#0.00");
            System.out.println(eurCurrency.currency + " kosztuje dzisiaj: " + eurRateValue + " zł. Za 100zł kupimy: " + formatter.format(100 / eurRateValue) + " " + (eurCurrency.code).toLowerCase());
        }
    }
}